package homework03


import kotlinx.coroutines.runBlocking


fun main(args: Array<String>) {
    runBlocking {
        val app = RedditClient()
        args.forEach {
            app.writeRedditSnapshot(it)
        }
    }
}