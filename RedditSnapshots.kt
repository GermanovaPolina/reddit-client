package homework03


import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.ktor.utils.io.errors.*


data class TopicSnapshot(val info: TopicInfo,
                         val posts: List<Post>) {
    val id: Int = hashCode()
    val created: Long = System.currentTimeMillis()
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class TopicInfo(val active_user_count: Int,
                     val subscribers: Int,
                     val public_description: String,
                     val created_utc: Long,
                     val title: String,
                     val id: String): RedditSnapshot

@JsonIgnoreProperties(ignoreUnknown = true)
data class Post(val selftext: String,
                val subreddit_id: String,
                val author: String,
                val ups: Int,
                val downs: Int,
                val created_utc: Long,
                val selftext_html: String?,
                val title: String,
                val num_comments: Int,
                val id: String) : RedditSnapshot

data class CommentsSnapshot(val comments: List<Comment>) {
    val id: Int = hashCode()
    val created: Long = System.currentTimeMillis()
}

class CommentsDeserializer : JsonDeserializer<Listing?>() {
    @Throws(IOException::class, JsonProcessingException::class)
    override fun deserialize(jp: JsonParser, ctxt: DeserializationContext?): Listing? {
        val rootNode: JsonNode = jp.readValueAsTree()
        val mapper = jacksonObjectMapper()
        mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true)

        if (rootNode.get("kind") == null) {
            return null
        }
        return Listing(mapper.readValue(rootNode.get("data").toString(), AfterBefore::class.java))
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class Comment(val created_utc: Long,
                   val ups: Int, val downs: Int,
                   val depth: Int,
                   val body: String,
                   val author: String,
                   val id: String,
                   val parent_id: String,
                   val subreddit_id: String,
                   @JsonDeserialize(using = CommentsDeserializer::class) val replies: Listing?) : RedditSnapshot

@JsonIgnoreProperties(ignoreUnknown = true)
data class MoreComments(val created_utc: Int,
                        val ups: Int,
                        val downs: Int,
                        val depth: Int,
                        val id: String,
                        val parent_id: String,
                        val children: List<String>) : RedditSnapshot

@JsonTypeInfo(use = JsonTypeInfo.Id.DEDUCTION)
@JsonSubTypes(
    JsonSubTypes.Type(value = TopicInfo::class),
    JsonSubTypes.Type(value = Comment::class),
    JsonSubTypes.Type(value = Post::class),
    JsonSubTypes.Type(value = MoreComments::class)
)

interface RedditSnapshot

@JsonIgnoreProperties(ignoreUnknown = true)
data class Kind(val data: RedditSnapshot)

@JsonIgnoreProperties(ignoreUnknown = true)
data class AfterBefore(val children: List<Kind>)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Listing(val data: AfterBefore) {
    fun getList(): List<Kind> = data.children
}
