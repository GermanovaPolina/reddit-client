package homework03


import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.serialization.jackson.*
import kotlinx.coroutines.*
import java.io.FileOutputStream


class RedditClient {

    private val httpClient = HttpClient(CIO) {
        install(ContentNegotiation) { jackson() }
    }
    private val urlGetter = RedditUrl()

    val greeting: String
        get() {
            return "Hello World!"
        }

    suspend fun getTopic(name: String): TopicSnapshot = coroutineScope {
        try {
            val info: Deferred<Kind> = async { httpClient.get(urlGetter.getInfo(name)).body() }
            val posts: Deferred<Listing> = async { httpClient.get(urlGetter.getPosts(name)).body() }
            TopicSnapshot(info.await().data as TopicInfo, posts.await().getList().map { it.data as Post })
        } catch (e: Exception) {
            println("Didn't manage to load Reddit :/")
            throw e
        }

    }

    private fun linearizeComments(comment: Comment): List<Comment> {
        if (comment.replies == null) {
            return listOf(comment)
        }
        val replies: List<List<Comment>> = comment.replies.getList().filter { it.data is
                Comment }.map { it.data as Comment }.map { linearizeComments(it) }
        return replies.flatten().plus(comment)
    }


    suspend fun getComments(id: String): CommentsSnapshot {
            try {
                val post: List<Listing> = httpClient.get(urlGetter.getComments(id)).body()
                return CommentsSnapshot(post[1].getList().filter { it.data is Comment }.map { it.data as Comment }.map {
                    linearizeComments(it) }.flatten())
            } catch (e: Exception) {
                println("Didn't manage to load Reddit :/")
                throw e
            }

    }

    private suspend fun writeTopic(topicSnapshot: TopicSnapshot) {
        withContext(Dispatchers.IO) {
            FileOutputStream("subjects.csv", true).bufferedWriter()
        }.use { out ->
            out.append("\"id\",\"created\"\n")
            out.append(topicSnapshot.id.toString() + "," + topicSnapshot.created.toString() + "\n")
            out.append(csvSerialize(listOf(topicSnapshot.info), TopicInfo::class))
            out.append(csvSerialize(topicSnapshot.posts, Post::class))
        }
    }

    private suspend fun writeComments(commentsSnapshot: CommentsSnapshot) {
        withContext(Dispatchers.IO) {
            FileOutputStream("comments.csv", true).bufferedWriter()
        }.use { out ->
            out.append("\"id\",\"created\"\n")
            out.append(commentsSnapshot.id.toString() + "," + commentsSnapshot.created.toString() + "\n")
            out.append(csvSerialize(commentsSnapshot.comments, Comment::class))
        }
    }

    suspend fun writeRedditSnapshot(subreddit: String) {
        coroutineScope {
            val topic = getTopic(subreddit)

            launch { writeTopic(topic) }
            launch {
                topic.posts.forEach {
                    if (it.num_comments != 0) {
                        val commentsSnapshot = getComments(it.id)
                        writeComments(commentsSnapshot)
                    }
                }
            }
        }
    }
}

