package homework03


class RedditUrl {
    fun getInfo(subreddit: String): String = "https://www.reddit.com/r/$subreddit/about.json"

    fun getPosts(subreddit: String): String = "https://www.reddit.com/r/$subreddit/.json"

    fun getComments(id: String): String = "https://www.reddit.com/comments/$id/.json"
}